# -*- coding: utf-8 -*-
# Example Mini-Project:
# THE MYSTICAL OCTOSPHERE!

# Пример вывода программы
#
# Your question was... Will I get rich?
# You shake the mystical octosphere.
# The cloudy liquid swirls, and a reply comes into view...
# The mystical octosphere says... Probably yes.
# 
# Your question was... Are the Cubs going to win the World Series?
# You shake the mystical octosphere.
# The cloudy liquid swirls, and a reply comes into view...
# The mystical octosphere says... Probably not.
#

# Прежде всего, импортируйте random модуль

# Вставьте команду импорта random модуля выше этой строки
# и убедитесь что у нее нет выравнивания

# Далее, напишите код функции number_to_fortune
# Это вспоманательная функция.
# Она принимает число и возвращает строку
# 
# Допустимые числа от 0 до 7 включительно
# (то есть числа 0, 1, 2, 3, 4, 5, 6, или 7)
# и каждое число должно соответствовать предсказанию
# которое послужит ответом да или нет вопроса
#
# Примеры предсказаний:
# 0 - Yes, for sure!
# 1 - Probably yes.
# 2 - Seems like yes...
# 3 - Definitely not!
# 4 - Probably not.
# 5 - I really doubt it...
# 6 - Not sure, check back later!
# 7 - I really can't tell
#
# Если каким-либо образом функция получила число, отличное от вышеуказанных
# Она должна вернуть строку, сообщающую об ошибке

def number_to_fortune(number):
    # Вставьте Ваш код здесь
    # Используйте if...elif...else конструкцию
    # для проверки значений на одно из допустимых 0...7
    # и возврата строки предсказаний
    

    
    
    pass # Важно! Удалите эту строку когда вставите Ваш код

    # Убедитесь, что весь Ваш код выровнен 4-мя пробелами

    
# TEST SECTION...    
# Раскомментируйте следующий код для тестирования функции number_to_fortune
#print
#print number_to_fortune(0)
#print number_to_fortune(1)
#print number_to_fortune(2)
#print number_to_fortune(3)
#print number_to_fortune(4)
#print number_to_fortune(5)
#print number_to_fortune(6)
#print number_to_fortune(7)
#print number_to_fortune(19)
#print
# Когда закончите тестирование функции number_to_fortune, закомментируйте код снова
    
    
# Теперь напишите код главной функции
# Она должна печатать вопрос,
# печатать строку чо Вы потрясли магический шар
# печатать что ответ появился
# и печатать предсказание
def mystical_octosphere(question):
    # Вставьте Ваш код ниже
    
    # Печать строки с вопросом
    

    # Печать строки "You shake the mystical octosphere."    
    
    
    # Используйте функцию randrange из модуля random для получения случайного числа между 0 и 7 
    # и поместите полученное число в переменную answer_number
    
    
    # Используйте вспомагательную функцию для конвертации
    # числа, полученного случайным образом выше
    # в предсказание и сохраните его 
    # в переменной под названием answer_fortune
    

    # Печать строки: "The cloudy liquid swirls, and a reply comes into view..."
    
    
    # Печать строки: "The mystical octosphere says..."
    # и предсказания, которое вы поместили в переменную answer_fortune
    

    # Печать пустой строки
    
    
    # Убедитесь, что весь Ваш код главной функции 
    # выровнен 4-мя пробелами
    
    # Удалите следующую строку когда вставите Ваш код
    pass

# следующие строки кода выполняют главную функцию
# Вы можете изменить вопросы по своему желанию
# Только "Да" или "Нет" вопросы имеют значение
mystical_octosphere("Will I get rich?")
mystical_octosphere("Are the Cubs going to win the World Series?")