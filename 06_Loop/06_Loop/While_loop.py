
# coding: utf-8

# The while loop is similar to an if statement: it executes the code inside of it if some condition is true. The difference is that the while loop will continue to execute as long as the condition is true. In other words, instead of executing if something is true, it executes while that thing is true.
# 
# Line 6 decides when the loop will be executed. So, "as long as count is less than 5," the loop will continue to execute. Line 8 increases count by 1. This happens over and over until count equals 5.
# 
# Instructions
# Change the loop so it counts up to 9 (inclusive).
# 
# Be careful not to change or remove the count += 1 bit—if Python has no way to increase count, your loop could go on forever and become an infinite loop which could crash your computer / browser!

# In[ ]:

count = 0

if count < 5:
    print "Hello, I am an if statement and count is", count
    
while count < 5:
    print "Hello, I am a while and count is", count
    count += 1


# Condition
# The condition is the expression that decides whether the loop is going to be executed or not. There are 5 steps to this program:
# The loop_condition variable is set to True
# The while loop checks to see if loop_condition is True. It is, so the loop is entered.
# The print statement is executed.
# The variable loop_condition is set to False.
# The while loop again checks to see if loop_condition is True. It is not, so the loop is not executed a second time.
# 
# Instructions
# See how the loop checks its condition, and when it stops executing? When you think you've got the hang of it

# In[ ]:

loop_condition = True

while loop_condition:
    print "I am a loop"
    loop_condition = False


# While you're at it
# Inside a while loop, you can do anything you could do elsewhere, including arithmetic operations.
# 
# Instructions
# Create a while loop that prints out all the numbers from 1 to 10 squared (1, 4, 9, 16, ... , 100), each on their own line.
# Fill in the blank space so that our while loop goes from 1 to 10 inclusive.
# Inside the loop, print the value of num squared. The syntax for squaring a number is num ** 2.
# Increment num.

# In[ ]:

num = 1

while _______:  # Fill in the condition
    # Print num squared
    # Increment num (make sure to do this!)


# Simple errors
# A common application of a while loop is to check user input to see if it is valid. For example, if you ask the user to enter y or n and they instead enter 7, then you should re-prompt them for input.
# 
# Instructions
# Fill in the loop condition so the user will be prompted for a choice over and over while choice does not equal 'y' and choice does not equal 'n'.

# In[ ]:

choice = raw_input('Enjoying the course? (y/n)')

while ________:  # Fill in the condition (before the colon)
    choice = raw_input("Sorry, I didn't catch that. Enter again: ")


# Infinite loops
# An infinite loop is a loop that never exits. This can happen for a few reasons:
# The loop condition cannot possibly be false (e.g. while 1 != 2)
# The logic of the loop prevents the loop condition from becoming false.
# 
# Example:
# 
# count = 10
# while count > 0:
#     count += 1 # Instead of count -= 1
# 
# Instructions
# The loop in the editor has two problems: it's missing a colon (a syntax error) and count is never incremented (logical error). The latter will result in an infinite loop, so be sure to fix both before running!

# In[ ]:

count = 0

while count < 10 # Add a colon
    print count
    # Increment count


# Break
# The break is a one-line statement that means "exit the current loop." An alternate way to make our counting loop exit and stop executing is with the break statement.
# 
# First, create a while with a condition that is always true. The simplest way is shown.
# 
# Using an if statement, you define the stopping condition. Inside the if, you write break, meaning "exit the loop."
# 
# The difference here is that this loop is guaranteed to run at least once.
# 
# Instructions
# See what the break does? Feel free to mess around with it (but make sure you don't cause an infinite loop)! 

# In[ ]:

count = 0

while True:
    print count
    count += 1
    if count >= 10:
        break


# While / else
# Something completely different about Python is the while/else construction. while/else is similar to if/else, but there is a difference: the else block will execute anytime the loop condition is evaluated to False. This means that it will execute if the loop is never entered or if the loop exits normally. If the loop exits as the result of a break, the else will not be executed.
# 
# In this example, the loop will break if a 5 is generated, and the else will not execute. Otherwise, after 3 numbers are generated, the loop condition will become false and the else will execute.
# 
# Instructions
# Code to see while/else in action!

# In[ ]:

import random

print "Lucky Numbers! 3 numbers will be generated."
print "If one of them is a '5', you lose!"

count = 0
while count < 3:
    num = random.randint(1, 6)
    print num
    if num == 5:
        print "Sorry, you lose!"
        break
    count += 1
else:
    print "You win!"


# Your own while / else
# Now you should be able to make a game similar to the one in the last exercise. The code from the last exercise is below:
# 
# count = 0
# while count < 3:
#     num = random.randint(1, 6)
#     print num
#     if num == 5:
#         print "Sorry, you lose!"
#         break
#     count += 1
# else:
#     print "You win!"
# 
# In this exercise, allow the user to guess what the number is three times.
# 
# guess = int(raw_input("Your guess: "))
# Remember, raw_input turns user input into a string, so we use int() to make it a number again.
# 
# Instructions
# Use a while loop to let the user keep guessing so long as guesses_left is greater than zero.
# Ask the user for their guess, just like the second example above.
# If they guess correctly, print 'You win!' and break.
# Decrement guesses_left by one.
# Use an else: case after your while loop to print You lose..

# In[ ]:

from random import randint

# Generates a number from 1 through 10 inclusive
random_number = randint(1, 10)

guesses_left = 3
# Start your game!

